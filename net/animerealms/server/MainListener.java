package net.animerealms.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.confuser.barapi.BarAPI;
import net.animerealms.server.blockTimer.*;
import net.animerealms.server.general.*;
import net.animerealms.server.messageBar.*;
import net.animerealms.server.sqlHandler.*;
import net.animerealms.server.stats.*;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;


@SuppressWarnings("unused")
public class MainListener extends JavaPlugin implements Listener {
	
	private boolean isEnabled = false;
	private Map<String,AnimePlayer> playerList = new HashMap<>();
	private Map<String,AnimeTeam> teamList = new HashMap<>();
	private StatListener statlist = new StatListener(this);
	private DummyPlayer dummyPlayer;


	public MainListener() {
	}
	
	public void onEnable(){
		getLogger().info("animeRealms enabled");
		getServer().getPluginManager().registerEvents(this,this);
		getServer().getPluginManager().registerEvents(statlist, this);
		getLogger().info("loaded players : " + loadPlayers());
		loadMonsters();
		ARExpReader.getInst();
		isEnabled = true;
	}

	public void onDisable(){
		getLogger().info("saved xp for players : " + saveXP());
		HandlerList.unregisterAll((Listener)this);
		HandlerList.unregisterAll(statlist);
		isEnabled = false;
		getLogger().info("animeRealms disabled");
		for(Player pl: getServer().getOnlinePlayers()) {
			String[] tempstr = {pl.getName()," ;s0,0,0,N"};
			BarParser.getInstance().cancelMove();
			BarParser.getInstance().setText(tempstr, this);
			BarAPI.removeBar(pl);
		}
	}
	
	private int saveXP() {
		int result = 0;
		for(AnimePlayer p: playerList.values()) {
			p.save();
			result++;
		}
		return result;
	}
	
	private void loadMonsters() {
		dummyPlayer = new DummyPlayer();
		
	}

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args){
		if (cmd.getName().equalsIgnoreCase("setWoodTimer")){
			BlockManager.getBlockManager(getLogger()).setTimeToLive(Integer.parseInt(args[0]));
			getLogger().info("the new time to live is :" + args[0] + " seconds");
		}
		if(cmd.getName().equalsIgnoreCase("togglewoodtimer")){
			getLogger().info("wood timer enabled :" + !isEnabled);
			isEnabled = !isEnabled;
		}
		if(cmd.getName().equalsIgnoreCase("logout")){
			getServer().getPlayer(sender.getName()).kickPlayer("You have logged out of" + " " + ChatColor.RED + ChatColor.BOLD +"Anime" +ChatColor.GREEN+ChatColor.BOLD +"Realms");
		}
		if(cmd.getName().equalsIgnoreCase("setMessage")){
			// /setmessage <player> <message>;s<#of moves>,<delay>,<speed>,L/N>
			BarParser.getInstance().setText(args, this);
		}		
		if(cmd.getName().equalsIgnoreCase("cancelMessage")){
				BarAPI.removeBar(getServer().getPlayer(args[0]));
		}	
		if(cmd.getName().equalsIgnoreCase("addFriend")) {
			getAPlayer(sender.getName()).addFriend(args[0]);
		}
		if(cmd.getName().equalsIgnoreCase("animeParty")) {
			if(args[0].equalsIgnoreCase("create") && !teamList.containsKey(sender.getName())) {
				teamList.put(sender.getName(), new AnimeTeam(playerList.get(sender.getName())));
				if(args[1] != null) {
					teamList.get(sender.getName()).setName(args[1]);
				}
			}
			if(args[0].equalsIgnoreCase("kick") &&teamList.containsKey(sender.getName())) {
				AnimeTeam t = teamList.get(sender.getName());
				for(int i = 1; i< args.length; i++) {
					t.kickMember(playerList.get(args[i]));
				}
			}
			if(args[0].equalsIgnoreCase("add" )&&teamList.containsKey(sender.getName())) {
				AnimeTeam t = teamList.get(sender.getName());
				for(int i = 1; i< args.length; i++) {
					t.addMember(playerList.get(args[i]));
				}
			}
			if(args[0].equalsIgnoreCase("disband") && teamList.containsKey(sender.getName())) {
				teamList.get(sender.getName()).disbandParty();
			}
		}
		if(cmd.getName().equalsIgnoreCase("setStats")) {
			ArrayList<Double> temp = new ArrayList<>();
			if(args.length < 6) {
				return true;
			}
			for(int i = 0; i<5;i++) {
				temp.add(Double.parseDouble(args[i+1]));
			}
			SQLInterface.getInst().updateStats(args[0], temp);
			if(playerList.containsKey(args[0])) {
				playerList.get(args[0]).setXp(temp);
			}
		}
		if(cmd.getName().equalsIgnoreCase("stats")) {
		//	System.out.println("executing command display stats in command listener");
			if(args.length>1) {
				displayStats(args[0]);
			}else {
				displayStats(sender.getName());
			}
		}
		
		return true;
	}
	
	private void displayStats(String username) {
		if(playerList.containsKey(username)) {
			playerList.get(username).displayStats();
		}
		
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent bl){
		
	//	System.out.println(bl.getPlayer().getItemInHand().getItemMeta().getDisplayName());
		
		if(BlockManager.getBlockManager(getLogger()).handleBlock(bl.getBlock()) && isEnabled)
		{
			bl.setCancelled(true);
		}else
			bl.setCancelled(false);
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent pl){
		//BarAPI.setMessage(pl.getPlayer(),"MOTD this is not my code");
		//BarAPI.hasBar(pl.getPlayer());
		Player player = pl.getPlayer();
	//	System.out.println(player.getName() + " has logged in");
		SQLInterface sqlInst = SQLInterface.getInst();
		playerList.put(pl.getPlayer().getName(),new  AnimePlayer(player, sqlInst.getStats(player.getName()), sqlInst.getFriends(player.getName())));		
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent pl) {
		AnimePlayer apl = getAPlayer(pl.getPlayer().getName());	
		apl.leaveParty();
		if(apl != null){
			apl.save();
			playerList.remove(apl);
		}
		String[] tempstr = {pl.getPlayer().getName()," ;s0,0,0,N"};
		BarParser.getInstance().setText(tempstr, this);
		BarAPI.removeBar(pl.getPlayer());
		
	}
	private int loadPlayers() {
		int result = 0;
		for(Player p: getServer().getOnlinePlayers()) {
			playerList.put(p.getName(), new AnimePlayer(p, SQLInterface.getInst().getStats(p.getName()), SQLInterface.getInst().getFriends(p.getName())));
			result++;
		}
		return result;
	}
	
	/*
	 * careful when pulling the dummyplayer, there might be many nullpointers
	 */
	public AnimePlayer getAPlayer(String name) {
		if(playerList.containsKey(name)) {
			return playerList.get(name);
		}else
			return dummyPlayer;
	}
}


package net.animerealms.server.stats;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

public class ARExpReader {
	
	private static ARExpReader inst = null;
	private int[] levels = new int[1000];
	private double[] dodge = new double[2001];
	
	public static ARExpReader getInst() {
		if(inst == null) {
			inst = new ARExpReader();
		}
		return inst;
	}
	
	private ARExpReader() {
		readLevels();
		readDodge();
	}

	private void readDodge() {
		Path fpath = Paths.get("dodge.txt");
		try {
			int i = 0;
			for(String line: Files.readAllLines(fpath, Charset.defaultCharset())) {
				dodge[i] = Double.parseDouble(line);
				//System.out.println("difference "+(i-1000)+" amounts to "+dodge[i]+" chance to dodge.");
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/*
	 * read the files that contains the exp per level.
	 * each line should be a pure int.
	 */
	private void readLevels() {
		Path fpath = Paths.get("levels.txt");
		try {
			int i = 0;
			for(String line: Files.readAllLines(fpath, Charset.defaultCharset())) {
				levels[i] = Integer.parseInt(line);
				//System.out.println("Level "+i+1+" amounts to "+levels[i]+" xp.");
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * this is an expensive operation, take care in abusing it
	 */
	public int getLevel(int exp) {
		for(int i = 0; i<levels.length;i++ ) {
			if(exp < levels[i]) {
				return i;  //Logically it should be i-1 but levels start at 1, not 0
			}
		}
		return 5;
	}
	
	/*
	 * returns how much of the xp to the next level has been gained
	 * can be used for any level and any xp
	 */
	
	public double progressToLevel(int currentLevel, Double currentxp) {
		double n;
		if(currentLevel >1) {
			 n = (double)levels[currentLevel-1];
		}else {
			 n = 0d;
		}
			double m = (double)levels[currentLevel];
		
		//System.out.println("n = " + n + "  m = "+ m + " experience = " + exp + " result = " + (((m-exp)/(m-n))));
			return (1-((m-currentxp)/(m-n)));
	}

	public int getExp(int i) {
		return levels[i];
	}
	
	public double getDodge(int delta) {
	 if(delta > 0 && delta < dodge.length) {
		 return dodge[delta];
	 }
	 return 0.0;
	}

}

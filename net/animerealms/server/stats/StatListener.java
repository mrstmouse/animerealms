package net.animerealms.server.stats;

import java.util.Random;

import net.animerealms.server.*;
import net.animerealms.server.general.AnimePlayer;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class StatListener implements Listener {

	private MainListener myBoss;
	private Random randGen = new Random();

	public StatListener(MainListener main) {
		myBoss = main;
	}

	/*
	 * get all the events of entities taking damage. If a player takes damage,
	 * then reduce that damage by the appropriate multiplier because players
	 * apparently gain hp as they level there is another multiplier This is also
	 * the DEX event, a miss will cancel the event
	 */
	@EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
	public void VITEvent(EntityDamageEvent e) {
		try {
		Entity damagee = e.getEntity();
		AnimePlayer aPlayer;
		if(damagee instanceof Player) {
			aPlayer = myBoss.getAPlayer(((HumanEntity) damagee).getName());
			aPlayer.gainXP(STAT.VIT, e.getDamage());
			e.setDamage(e.getDamage() * (aPlayer.getHPMulti())* (aPlayer.getMultiplier(STAT.VIT)));
		}
		}catch(ClassCastException exc) {
			System.out.println("wrong cast");
		}

	}

	/*
	 * get all the events of entities dealing damage to entities if a player
	 * deals damage, the damage is amplified by the appropriate multiplier
	 */
	@EventHandler(priority = EventPriority.LOW)
	public void STREvent(EntityDamageByEntityEvent e) {
		Entity damager = e.getDamager();
		Entity damagee = e.getEntity();
		AnimePlayer aDamager;
		AnimePlayer aDamagee;
		if(damager instanceof Player) {
			aDamager = myBoss.getAPlayer(((Player) damager).getName());
		}else {
			 aDamager = myBoss.getAPlayer("DummyNotch");
		}
		if(damagee instanceof Player) {
			aDamagee = myBoss.getAPlayer(((Player) damagee).getName());
		}else {
			aDamagee = myBoss.getAPlayer("DummyNotch");
		}
		double hitC = ARExpReader.getInst().getDodge(
				(int) (1000 + aDamager.getMultiplier(STAT.DEX) - aDamagee
						.getMultiplier(STAT.DEX)));
		e.setDamage(e.getDamage() * aDamager.getMultiplier(STAT.STR));
		/*if((aDamager.getBukkitPlayer().getInventory().getItemInHand().getItemMeta().getDisplayName().contains("AR"))) {
			ARWeapon weap = (ARWeapon)(aDamager.getBukkitPlayer().getInventory().getItemInHand().);
			
		}*/
		if (hitC * 100 > randGen.nextInt(100)) {
			aDamagee.gainXP(STAT.DEX, e.getDamage());
			e.setCancelled(true);
			
			return;
		} else {
			aDamager.gainXP(STAT.STR, e.getDamage());
		}
		
	}

	/*
	 * Lets assume only players have food level, this might bite me in the ass
	 * get all the food level change events. If its a players, add the right
	 * amount of food to the food level The food level is discrete, so the
	 * amount of STA is tracked in the player class
	 */
	@EventHandler
	public void STAFoodEvent(FoodLevelChangeEvent e) {
		if(e.getEntity().getType().equals(EntityType.PLAYER)) {
			e.setFoodLevel((int) (e.getFoodLevel() + myBoss.getAPlayer(
					e.getEntity().getName()).getFood()));
		}
	}

	/*
	 * a player can heal faster and better with more stamina so get all the
	 * health regain events and then multiply
	 */
	@EventHandler
	public void STAHealEvent(EntityRegainHealthEvent e) {
		Entity ent = e.getEntity();
		if (ent.getType().equals(EntityType.PLAYER)) {
			e.setAmount(e.getAmount()
					* myBoss.getAPlayer(((Player) ent).getName())
							.getMultiplier(STAT.STA));
		}
	}
	
	/*
	 * STA xp event
	 */
	@EventHandler
	public void STAXPevent(FoodLevelChangeEvent e) {
		Entity ent = e.getEntity();
			if(ent instanceof Player) {
				AnimePlayer aPlayer = myBoss.getAPlayer(((Player) ent).getName());
				int dFood = aPlayer.getBukkitPlayer().getFoodLevel() - e.getFoodLevel();
				if(dFood >0) {
					aPlayer.gainXP(STAT.STA, dFood);
				}
				
			}
	}

}

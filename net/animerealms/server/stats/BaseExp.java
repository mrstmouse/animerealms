package net.animerealms.server.stats;

public class BaseExp {
	
	private final static int BASELEVEL = 70;
	
	public static int getBaselevel() {
		return BASELEVEL;
	}

	public static double baseStr() {
		System.out.println("base str multiplier = " +(double)BASELEVEL/300 );
		return (double)BASELEVEL/300;
	}
	
	public static double baseVit() {
		return (double)BASELEVEL/50;
	}
	
	public static double baseDex() {
		return (double)BASELEVEL/50;
	}
	
	public static double baseSta() {
		return (double)BASELEVEL/70;
	}
	
	public static double baseLuck() {
		return (double)BASELEVEL/70;
	}

	public static double getBase(STAT stat) {
		System.out.println("delivering base for " + stat.toString());
		double temp = 0d;
		switch (stat) {
		case DEX:
			temp = baseDex();
			break;
		case STR:
			temp =  baseStr();
			break;
		case STA:
			temp= baseSta();
			break;
		case VIT:
			temp= baseVit();
			break;
		case LUCK:
			temp= baseLuck();
			break;
		default:
			break;
		}
		return temp;
	}
}

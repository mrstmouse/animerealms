package net.animerealms.server.blockTimer;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;


public class BlockManager {
	
	private static BlockManager manager = null;
	private static Logger myLogger;
	private List<MyBlock> blList = new LinkedList<MyBlock>();
	private int TTL = 60; //time inbetween cutting
	private int TPS = 60; //ticks per second

	
	/*
	 * 
	 */
	public static BlockManager getBlockManager(Logger logger){
		if (manager == null){
			manager = new BlockManager();
		}
		if(myLogger == null){
			myLogger = logger;
		}
		return manager;
	}
	
	public void setTimeToLive(int ttl){
		TTL = ttl;
	}
	private BlockManager() {
		
	}
	public boolean handleBlock(Block block) {
		dumpOldBlocks();
		if(block.getType() == Material.LOG){
			for(MyBlock bl: blList){
				if(bl.isblock(block)){
					myLogger.info("that log was recently cut");
					return true;
				}
			}
			blList.add(new MyBlock(block));
			block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.LOG, 1));
			myLogger.info("sucessfully cut a log");
			return true;
		}
		return false;
	}
	private void dumpOldBlocks() {
		for(int i = 0; i < blList.size();i++){
			MyBlock bl = blList.get(i);
			if(bl.getTime() > TTL*TPS){
				blList.remove(bl);
			}else{
				break;
			}
		}
		
	}
}

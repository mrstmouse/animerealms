package net.animerealms.server.blockTimer;

import org.bukkit.block.Block;

public class MyBlock {
	
	private Block parentBlock = null;
	private long startTime = 0;

	public MyBlock(Block nb) {
		parentBlock = nb;
		startTime = parentBlock.getWorld().getFullTime();
	}
	
	public boolean isblock(Block rhs){
		return (parentBlock.getX() == rhs.getX() && parentBlock.getY() == rhs.getY() && parentBlock.getZ() ==rhs.getZ());
	}
	
	public long getTime(){
		return parentBlock.getWorld().getFullTime()-startTime;
	}

}

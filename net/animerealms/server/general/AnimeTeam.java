package net.animerealms.server.general;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class AnimeTeam {
	
	private Scoreboard board;
	private Team team;
	private Objective obj;
	private AnimePlayer leader;
	private List<AnimePlayer> members = new LinkedList<>();
	private boolean FF = false;
	private boolean allowedToLeave = true;

	public AnimeTeam(AnimePlayer leader, String partyName) {
		this.leader = leader;
		initiateTeam(partyName);
	}
	
	public AnimeTeam(AnimePlayer leader) {
		this.leader = leader;
		initiateTeam(leader.getBukkitPlayer().getName()+"'s party");
	}
	
	public boolean isLeader(AnimePlayer c) {
		return leader.equals(c);
	}
	
	public boolean isMember(AnimePlayer c) {
		return members.contains(c);
	}
	
	private void initiateTeam(String partyName) {
		if(partyName.length() > 16) {
		partyName = partyName.substring(0, 15);
		}
		Player pLeader = leader.getBukkitPlayer();
		ScoreboardManager man = Bukkit.getScoreboardManager();
		board = man.getNewScoreboard();
		team = board.registerNewTeam(partyName);
		team.addPlayer(pLeader);
		team.setAllowFriendlyFire(FF);
		obj = board.registerNewObjective("test", "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName("Player | HP");
		obj.getScore(pLeader).setScore((int) pLeader.getHealth());
		pLeader.setScoreboard(board);
		
	}
	
	public void addMember(AnimePlayer m) {
		if(m != null && !leader.equals(m) && !members.contains(m) && m.joinParty(this)) {
			Player member = m.getBukkitPlayer();
			team.addPlayer(member);
			member.setScoreboard(board);
			obj.getScore(member).setScore((int)member.getHealth());
			members.add(m);
		}
	}
	
	public void kickMember(AnimePlayer a) {
		if(a!= null && members.contains(a)) {
			Player member = a.getBukkitPlayer();
			team.removePlayer(member);
			member.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			members.remove(a);
		}
	}
	
	public void disbandParty() {
		for(AnimePlayer m: members) {
			Player member = m.getBukkitPlayer();
			team.removePlayer(member);
			member.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			members.remove(m);
		}		
		Player pLeader = leader.getBukkitPlayer();
		team.removePlayer(pLeader);
		pLeader.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
	}
	
	public void toggleFF() {
		team.setAllowFriendlyFire(!FF);
		FF = !FF;
	}

	public boolean requestLeave(AnimePlayer a) {
		if(allowedToLeave) {
			kickMember(a);
			return true;
		}
		return false;
	}
	
	public void toggleAllowedToLeave() {
		allowedToLeave = !allowedToLeave;
	}

	public void setName(String s) {
		team.setDisplayName(s);
		
	}
}

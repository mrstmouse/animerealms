package net.animerealms.server.general;

import java.util.ArrayList;

import net.animerealms.server.sqlHandler.*;
import net.animerealms.server.stats.*;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
public class AnimePlayer {
	
	private Player bukkitPlayer;
	private ArrayList<Double> xp;
	private int[] levels = {1,1,1,1,1};
	private double[] multipliers = {1,1,1,1,1};
	private ArrayList<String> friendlist;
	private AnimeTeam myParty;
	private int foodOverflow = 40, foodCtr = 0;
	
	public AnimePlayer(Player p, ArrayList<Double> xp, ArrayList<String> friends) {
		setBukkitPlayer(p);
		setFriendlist(friends);
		if(SQLInterface.getInst().getUserDBID(p.getName())==0) {
			SQLInterface.getInst().addUser(p.getName());
		}
		if(xp.size()<2) {
			ArrayList<Double> t = new ArrayList<>();
			for(int i = 0; i <5;i++) {
				t.add(1d);
			}
			setXp(t);
			SQLInterface.getInst().addUserStats(p.getName(), t);
		}else {
			setXp(xp);
		}
	}
	
	public AnimePlayer() {
		ArrayList<Double> t = new ArrayList<>();
		for(int i = 0; i <5;i++) {
			t.add(1d);
		}
		setXp(t);
	}

	public ArrayList<String> getFriendlist() {
		return friendlist;
	}

	public void setFriendlist(ArrayList<String> friendlist) {
		this.friendlist = friendlist;
	}

	public ArrayList<Double> getXp() {
		return xp;
	}

	public void setXp(ArrayList<Double> Newxp) {
		if(xp == null) {
			xp = Newxp;
		}
		ARExpReader expinst = ARExpReader.getInst();
		int i = 0;
		for(Double p: Newxp) {
			xp.set(i,p);
			levels[i] = expinst.getLevel(p.intValue());
			i++;
		}
		updateMultipliers();
	}
	/**
	 * 
	 * @param i STR, DEX, VIT, STA, LUCK
	 * @param exp
	 */
	public void levelUp(STAT stat, Double newExp) {
		int i = stat.ordinal();
		int oldTotal = (levels[0] + levels[1]+levels[2]+levels[3]+levels[4])/5;
		levels[i]++;
		xp.set(i, newExp);
		updateMultipliers();
		int totalLevel = (levels[0] + levels[1]+levels[2]+levels[3]+levels[4])/5;
		if(oldTotal < totalLevel) {
			displayTotalLevelUp(totalLevel);
		}
		bukkitPlayer.sendMessage(ChatColor.RED + ""+ ChatColor.BOLD +stat.toString()+ChatColor.GOLD + " is now level "+ChatColor.BLUE +levels[i] );
		
	}

	private void displayTotalLevelUp(int newLevel) {
		bukkitPlayer.sendMessage(ChatColor.GOLD+""+ChatColor.BOLD+""+ChatColor.ITALIC+"You have Leveled up! You are now Level " + newLevel + "!");
		bukkitPlayer.sendMessage("----------------------------------------------------");
		sendStatsLine(0, "STR ");
		sendStatsLine(1, "DEX ");
		sendStatsLine(2, "VIT ");
		sendStatsLine(3, "STA ");
		sendStatsLine(4, "LUK ");
	}

	public Player getBukkitPlayer() {
		return bukkitPlayer;
	}

	public void setBukkitPlayer(Player bukkitPlayer) {
		this.bukkitPlayer = bukkitPlayer;
	}

	public void addFriend(String friend) {
		if(!friendlist.contains(friend)) {
			SQLInterface.getInst().addFriend(bukkitPlayer.getName(), friend);
		}
	}

	public void save() {
		SQLInterface.getInst().updateStats(bukkitPlayer.getName(), getXp());
		
	}
	
	public boolean leaveParty() {
		if(myParty != null) {
			if(myParty.isLeader(this)) {
				myParty.disbandParty();
				return true;
			}else if(myParty.requestLeave(this)){
				return true;
			}else
				return false;
		}
		return true;
	}
	
	public boolean joinParty(AnimeTeam t) {
		if(myParty == null || leaveParty() ) {
			myParty = t;
			return true;
		}
		return false;
	}

	public void displayStats() {
	//	System.out.println("sending stats for " +bukkitPlayer.getName() );
		bukkitPlayer.sendMessage(ChatColor.AQUA+""+ChatColor.BOLD+""+ChatColor.ITALIC+"Stats");
		sendStatsLine(-1, "Total Level");
		sendStatsLine(0, "STR ");
		sendStatsLine(1, "DEX ");
		sendStatsLine(2, "VIT ");
		sendStatsLine(3, "STA ");
		sendStatsLine(4, "LUK ");
		
	}

	private void sendStatsLine(int i, String string) {
		if(i >= 0) {
		StringBuilder strb = new StringBuilder(ChatColor.BOLD+ string);
		Double currentxp = xp.get(i);
			int nextlvl = levels[i];
			//int currlvl = Math.max(0, nextlvl-1);
			//int nextLevelExp = ARExpReader.getInst().getExp(nextlvl);
			//int currentLevelExp = ARExpReader.getInst().getExp(currlvl);
			int e = (int) (10*ARExpReader.getInst().progressToLevel(nextlvl, currentxp));
		//	System.out.println("e = " + e + " @ "+ i);
			strb.append(ChatColor.BOLD+"::["+ChatColor.RESET);
			strb.append(ChatColor.GOLD);
			appendBar(strb, e);
			strb.append(ChatColor.BLACK);
			appendBar(strb, 10-e);
			strb.append(ChatColor.WHITE+""+ChatColor.BOLD +"]:: "+ChatColor.RESET+/* Math.max((currentxp-currentLevelExp),0)+"/"+(nextLevelExp-currentLevelExp) +*/ " Level " + nextlvl);
			bukkitPlayer.sendMessage(strb.toString());
		}else {
			int sum = 0;
			for(int j: levels) {
				sum += j;
			}
			sum = (int)sum/5;
			bukkitPlayer.sendMessage(ChatColor.GOLD+""+ChatColor.BOLD+"Total Level: "+ ChatColor.BLUE + sum);
		}
	}

	private void appendBar(StringBuilder strb, int e) {
		if(e > 10) {
			e =10;
		}
		for(int t = 0;t<e;t++) {
			strb.append("=");
		}
	}

	public double getMultiplier(STAT stat) {
		return multipliers[stat.ordinal()];
		
	}
	
	private void updateMultipliers() {
		//STR
		if(levels[0]<1000) {
			multipliers[0] = 1+0.025*levels[0];
		}
		//VIT
		if(levels[2]<200) {
			multipliers[2] = 1-0.0006667*levels[2]+0.06667;
		}else {
			multipliers[2] = 1-0.0005*levels[2]+0.1;
		}
		//STA
		if(levels[3]<1000) {
			multipliers[3] = 1+0.003*levels[3];
		}
		setFoodparams();
		//DEX
		multipliers[2] = levels[2];
	}

	private void setFoodparams() {
		if(bukkitPlayer != null) {
			bukkitPlayer.setFoodLevel(20);
			foodCtr = 0;
			foodOverflow = 40-(int)(levels[3]*0.040);
		}
		
	}

	public int getFood() {
		foodCtr++;
		if(foodOverflow<=foodCtr) {
			foodCtr = 0;
			return 1;
		}
		return 0;
	}
	
	public double getHPMulti() {
		int total = 0;
		for(int i:levels) {
			total+= i;
		}
		total = (int)(total/5);
		return 666/(5*(1333*total-1000));
	}

	/*
	 * For STR: STR Multiplied damage, not called if dodged
	 * For DEX: STR Multiplied damage, called after cancel event
	 * FOR VIT: STR Multiplied damage, not called if dodged, called before VIT reduction
	 * FOR STA: called when the food level diminishes
	 */
	public void gainXP(STAT stat, double v) {
		ARExpReader reader= ARExpReader.getInst();
		int ord = stat.ordinal();
		int endOfLvl = reader.getExp(levels[ord]);
		System.out.println(bukkitPlayer.getName() + " got " + v + " XP in " + stat.toString() + "! The old xp was "+xp.get(0) +" and also dis v * base =  : " +v + " * "+ BaseExp.getBase(stat) + " = "+(v*BaseExp.getBase(stat)));
		Double newExp = (v*BaseExp.getBase(stat) + xp.get(ord));
		if(newExp>endOfLvl) {
			levelUp(stat,newExp);
		}else {
			System.out.println("adding new xp : " + newExp);
			xp.set(ord,newExp);
			System.out.println((""));
		}
		
	}
}

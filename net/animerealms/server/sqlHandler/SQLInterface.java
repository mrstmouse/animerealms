package net.animerealms.server.sqlHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SQLInterface {

	private static SQLInterface inst = null;
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost/animerealms";
	private String user = "animerealms", pass = "\"h=2csxK\"Ff4@Q3",
	//private static String user = "test", pass = "test",
			DBusernames = "usernames", DBfriends = "friendlist",
			DBstats = "user_stats";
	private Connection con = null;
	private Statement st = null;
	private ResultSet rs = null;
	//private static final int NR_OF_STATS = 3;

	public static SQLInterface getInst() {
		if (inst == null) {
			inst = new SQLInterface();
		}
		return inst;
	}

	private SQLInterface() {
		loadDriver();
	}

	private void loadDriver() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("driver not loaded!");
			e.printStackTrace();
		}

	}

	/*
	 * returns the friendlist for a user
	 */
	public ArrayList<String> getFriends(String username) {
		ArrayList<String> result = new ArrayList<>();
		String query = "SELECT buddy FROM " + DBusernames + " JOIN "
				+ DBfriends + " on " + DBusernames + ".id = " + DBfriends
				+ ".id WHERE username = '" + username + "'";
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
			rs = st.executeQuery(query);
			while (rs.next()) {
				result.add(rs.getString(1));
			}
		} catch (SQLException e) {
			System.out.println("Get Friends SQL Error " + e);

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				if (con != null)
					con.close();

			} catch (SQLException e) {
				System.out.println("SQL error " + e);
			}
		}

		return result;
	}

	public String getUsername(int id) {
		String result = "";
		String query = "SELECT username FROM "+DBusernames+"  WHERE id = "
				+id;
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
			rs = st.executeQuery(query);
			while (rs.next()) {
				result = (rs.getString(1));
			}
		} catch (SQLException e) {
			System.out.println("SQL Error " + e);

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				if (con != null)
					con.close();

			} catch (SQLException e) {
				System.out.println("SQL error " + e);
			}
		}
		return result;
	}
	public void addFriend(String username, String friend) {
		String insertString = "insert into friendlist(id,buddy) values("
				+ getUserDBID(username) + ",'" + friend + "')";
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
			st.executeUpdate(insertString);
		} catch (SQLException e) {
			System.out.println("Add Friend SQL Error" + e);
		}
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
		} catch (SQLException e) {
			System.out.println("SQL Error" + e);
		}

	}

	public int getUserDBID(String username) {
		int result = 0;
		String query = "SELECT id FROM usernames  WHERE username = '"
				+ username + "'";
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
			rs = st.executeQuery(query);
			while (rs.next()) {
				result = (rs.getInt(1));
			//	System.out.println("this user was selected : " + result);
			}
		} catch (SQLException e) {
			System.out.println("SQL Error " + e);

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				if (con != null)
					con.close();

			} catch (SQLException e) {
				System.out.println("SQL error " + e);
			}
		}

		return result;
	}

	public ArrayList<Double> getStats(String username) {
		ArrayList<Double> temp = new ArrayList<>();
		String query = "SELECT STR,DEX,VIT,STA,LUCK FROM " + DBstats + " WHERE id = "
				+ getUserDBID(username);
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
			rs = st.executeQuery(query);
			while (rs.next()) {
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					temp.add(new Double(rs.getInt(i)));
				}
			}
		} catch (SQLException e) {
			System.out.println("Get Friends SQL Error " + e);

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				if (con != null)
					con.close();

			} catch (SQLException e) {
				System.out.println("SQL error " + e);
			}
		}

		return temp;

	}

	public void updateStats(String username, ArrayList<Double> arrayList) {
		String insertString = "UPDATE "+DBstats+" SET STR=" + arrayList.get(0).intValue() +",DEX=" +arrayList.get(1).intValue()+",VIT="+arrayList.get(2).intValue()+",STA="+arrayList.get(3).intValue()+",LUCK="+arrayList.get(4).intValue()+" WHERE id ="+ getUserDBID(username);
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
			System.out.println(insertString);
			st.executeUpdate(insertString);
		} catch (SQLException e) {
			System.out.println("Update stats SQL Error" + e);
		}
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
		} catch (SQLException e) {
			System.out.println("SQL Error" + e);
		}
	}

	public void addUser(String username) {
		String insertString = "insert into "+DBusernames+"(username) values('" + username + "')";
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
		//	System.out.println(insertString);
			st.executeUpdate(insertString);
		} catch (SQLException e) {
			System.out.println("Add Friend SQL Error" + e);
		}
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
		} catch (SQLException e) {
			System.out.println("SQL Error" + e);
		}
		
	}

	public void addUserStats(String username, ArrayList<Double> t) {
		String insertString = "insert into "+DBstats+"(id,STR,DEX,VIT,STA,LUCK) values("+getUserDBID(username)+"," + t.get(0).intValue() + "," + t.get(1).intValue() + "," + t.get(2).intValue() + "," + t.get(3).intValue() + "," + t.get(4).intValue() + ")";
		try {
			con = DriverManager.getConnection(url, user, pass);
			st = con.createStatement();
		//	System.out.println(insertString);
			st.executeUpdate(insertString);
		} catch (SQLException e) {
			System.out.println("Add Stats SQL Error :" + e);
		}
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
		} catch (SQLException e) {
			System.out.println("SQL Error" + e);
		}
		
	}
}

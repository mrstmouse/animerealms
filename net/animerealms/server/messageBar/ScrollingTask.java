package net.animerealms.server.messageBar;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ScrollingTask extends BukkitRunnable {
	private int counter;
	private boolean infinite = false;
	private Player player;
	
	public ScrollingTask( int c, Player p) {
		infinite = false;
		player =p;
		if(c < 0) {
			infinite = true;
		}
		counter = c;
		//System.out.println("infinite is " + infinite +"and c is " + c);
	}

	@Override
	public void run() {
		BarParser.getInstance().MoveMessage(player);
		if((counter-- < 0 && !infinite) || !BarParser.getInstance().isMoving()) {
			this.cancel();
		}
		
	}
	
	public void cancelScroll() {
		this.cancel();
	}

}

package net.animerealms.server.messageBar;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import me.confuser.barapi.BarAPI;

public class BarParser {
	
	private static BarParser  inst = null;
	private Map<Character,ChatColor> dict = new HashMap<Character,ChatColor>();
	private String currentMsg = "", restMsg =" ";
	private boolean isMoving = false;
	@SuppressWarnings("unused")
	private BukkitTask scroller;
	private int[] scrollingParams = {100,0,5}; //number of frames, delay, period
	
	private BarParser(){
		populateDict();
	}
	
	private void populateDict() {
		dict.put('0', ChatColor.BLACK);
		dict.put('1', ChatColor.DARK_BLUE);
		dict.put('2', ChatColor.DARK_GREEN);
		dict.put('3', ChatColor.DARK_AQUA);
		dict.put('4', ChatColor.DARK_RED);
		dict.put('5', ChatColor.DARK_PURPLE);
		dict.put('6', ChatColor.GOLD);
		dict.put('7', ChatColor.GRAY);
		dict.put('8', ChatColor.DARK_GRAY);
		dict.put('9', ChatColor.BLUE);
		dict.put('A', ChatColor.GREEN);
		dict.put('B', ChatColor.AQUA);
		dict.put('C', ChatColor.RED);
		dict.put('D', ChatColor.LIGHT_PURPLE);
		dict.put('E', ChatColor.YELLOW);
		dict.put('F', ChatColor.WHITE);
		dict.put('M', ChatColor.STRIKETHROUGH);
		dict.put('N', ChatColor.UNDERLINE);
		dict.put('L', ChatColor.BOLD);
		dict.put('O', ChatColor.ITALIC);
		dict.put('R', ChatColor.RESET);
	}

	public static BarParser getInstance(){
		if(inst == null){
			inst = new BarParser();
		}
		return inst;
	}
	
	public void cancelMove() {
		isMoving = false;
	}
	
	public void setText(String[] input, JavaPlugin pl){
		currentMsg ="";
		restMsg =" ";
		Player playerName = pl.getServer().getPlayer(input[0]);
		input[0] = "";
		String tmpStr = parseString(input);
		if(tmpStr.length() > 64) {
			currentMsg = tmpStr.substring(0, 63);
			restMsg = tmpStr.substring(63);
		}else
			currentMsg = tmpStr;
		if(isMoving) {
			scroller = new ScrollingTask(scrollingParams[0], playerName).runTaskTimer(pl, scrollingParams[1], scrollingParams[2]);
		}
		BarAPI.setMessage(currentMsg);
	}
	
	private String parseString(String[] in) {
		String sb = "";
		for(String str: in) {
			sb += str + " ";
		}
		String[] splitStr = sb.split("&");
		sb = "";
		for(String str: splitStr) {
			if(str.length() > 0 && dict.containsKey(str.charAt(0))) {
				sb += (dict.get(str.charAt(0)) + str.substring(1));
			}else
				sb+=str;
		}
		splitStr = sb.split(";");
		isMoving = false;
		try {
		if(splitStr.length > 1 && splitStr[1].contains("s")) {
						String[] tmpArgs = splitStr[1].split("s")[1].split(",");
						scrollingParams[0] = Integer.parseInt(tmpArgs[0]);
						scrollingParams[1] = Integer.parseInt(tmpArgs[1]);
						scrollingParams[2] = Integer.parseInt(tmpArgs[2]);
						if(splitStr[1].indexOf("L")>-1) {		
							scrollingParams[0] = -1;
						}
						isMoving = true;
		}
		}
		 catch(NumberFormatException e) {
			 System.out.println(e);
		 }
		return new String(splitStr[0]+ChatColor.RESET);
	}

	public void MoveMessage(Player player) {
		if(currentMsg.length() < 63) {
			currentMsg += " ";	
		}else {	
			if(currentMsg.charAt(0) != ChatColor.COLOR_CHAR) {
				restMsg += currentMsg.charAt(0);
				currentMsg = currentMsg.substring(1);
				currentMsg += restMsg.charAt(0);
				restMsg = restMsg.substring(1);
			}
			while(currentMsg.charAt(0) == ChatColor.COLOR_CHAR) {
				restMsg += currentMsg.charAt(0);
				restMsg += currentMsg.charAt(1);
				currentMsg = currentMsg.substring(2);
				currentMsg += restMsg.charAt(0);
				currentMsg += restMsg.charAt(1);
				restMsg = restMsg.substring(2);
			}
		}
			BarAPI.setMessage(player,currentMsg);
	}
	
	public boolean isMoving() {
		return isMoving;
	}

}
